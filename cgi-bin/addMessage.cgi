#!/usr/bin/perl
#===========================================================================
# File name: addMessage.cgi
# Author:    Philip Gedgaud
# Purpose:   Adds the users message to the messages file
# Date:      12/12/10
#===========================================================================

use CGI;
use HTML::Scrubber;

my $cgi = new CGI;
my $scrubber = HTML::Scrubber->new();

#Start the HTML File
print $cgi->header();
print $cgi->start_html(-title=>"Philip's Student Messageboard");

#Open the file for writing out
$theFile="../lab/messageBoard/messages";
open(FILEOUT,">>$theFile") || warn "Could not open messages file.\n";         

#Scrub and Get the parameters
$author = $scrubber->scrub($cgi->param("author"));
$email = $scrubber->scrub($cgi->param("email"));
$messageBody = $scrubber->scrub($cgi->param("messageBody"));
$messageBody =~ s/\cM//g;
$messageBody =~ s/\n/<br \/>/g;
$humanTime = localtime;
$time = time;
$found = $cgi->param("found");
$subject = $scrubber->scrub($cgi->param("subject"));
$ipAddress= $ENV{'REMOTE_ADDR'};

#Preform a line count and creates an array of the messages with each message in each element of array
@allMessages;
$lineCount = 0;
open (FILEIN, "../lab/messageBoard/messages") || warn "Could not open messages file\n";
while (<FILEIN>)
        {
            $lineCount = $lineCount + 1;
            chomp;
            push(@allMessages, $_);
        }
close(FILEIN);

$messageOKtoPrint = 0;

#Run through the array and test to see if any messages have been submitted in last 
#60 seconds if the have test to see if they are same author or ip address
#of current submitter
for ($count = 0; $count < $lineCount; $count++) 
  { 

	@message = split(/\|/, $allMessages[$count]);
	$timedif = $time - $message[7];	
	#print "$timedif<br />";    	
	if($timedif < 60)
	 {	
		#if the ip address has submitted an message in the last minute
		if ($ipAddress eq $message[8])
	   	 {
			#Give back error message
			print "<h1>Sorry your message was not submitted.<br />Please wait 60 seconds before submitting another message.</h1>";
			$messageOKtoPrint = 0;
			last;
		 }		

		# if users has entered a message in last minute
		elsif ($author eq $message[1])
		 {
                        #Give back error message
                        print "<h1>Sorry your message was not submitted.<br />Please wait 60 seconds before submitting another message.</h1>";
			$messageOKtoPrint = 0;
			last;

		 }

		else
		 {
			#Write to the file then close
           		$messageOKtoPrint = 1;
		 } 
	 }
	
	else
	 {
		$messageOKtoPrint = 1;
	 }      
 }    

#Prints message if not same author or ip address 
if($messageOKtoPrint == 1)
 {
	print FILEOUT "$subject\|$author\|$email\|$messageBody\|$humanTime\|$found\|$time\|$ipAddress\n";
 }	
  

close(FILEOUT);
print "<a href=\"https://www.philipgedgaud.com/cgi-bin/project1.cgi\">Back to Messageboard</a>";
print $cgi->end_html();
