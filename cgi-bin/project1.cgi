#!/usr/bin/perl
#===========================================================================
# File name: project1.cgi
# Author:    Philip Gedgaud
# Purpose:   Setup a message board that displays messages in the browser and
#            allows the user to add there own message.
# Date:      12/12/10
#===========================================================================

use CGI;
use POSIX;

my $cgi = new CGI;

#Start the HTML File
print $cgi->header();
print "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
print "<html lang=\"en-US\" xml:lang=\"en-US\" xmlns=\"http://www.w3.org/1999/xhtml\">";
print "<head>";
print "<title>Philip's Messageboard</title>";
print "<meta http-equiv=\"Content-Type\" content=\"text/html\; charset=iso-8859-1\" />";
print "<link rel=\"stylesheet\" type=\"text/css\" href=\"../lab/messageBoard/style.css\" />";
print "</head>";
print "<body>";
print "<a href=\"http://www.philipgedgaud.com/lab/\">Back</a>";
print "<h1>Project One Messageboard.</h1>\n";
print "<div id=\"addMessage\">\n";
print "<form action=\"http://www.philipgedgaud.com/cgi-bin/addMessage.cgi\" method=\"post\">\n";
print "Subject:<br /> <input type=\"text\" name=\"subject\"/><br />\n";
print "Name: <br /><input type=\"text\" name=\"author\"/><br />\n";
print "Email: <br /><input type=\"text\" name=\"email\" /><br />\n";
print "Message:<br /> <textarea rows=\"10\" cols=\"30\" name =\"messageBody\"></textarea><br />\n";
print "How did you hear about this message board?<br />";
print "<select name=\"found\">";
print "<option value=\"friend\">From a Friend</option>";
print "<option value=\"search\">From a search engine.</option>";
print "<option value=\"stumbled\">I stumbled upon it.</option>";
print "<option value=\"other\">Other</option>";
print "</select><br />";
print "<input type=\"submit\" value=\"Submit Message\" />";
print "</form>\n";
print "</div> <!-- End of addMessage div -->\n";
print "<div id=\"allMessages\">";

#Read in the file and display each message
open (FILEIN, "../lab/messageBoard/messages") || warn "Could not open messages file\n";

$lineCount = 0;

#Performs a line count and creates an array of the messages with each message in each element of array
@allMessages;
while (<FILEIN>)
	{
            $lineCount = $lineCount + 1;
            chomp;    
            push(@allMessages, $_);
	}

#Figures out the number of pages needed for all of the messages
$numOfPages = ceil($lineCount/5);

#Creates a link for each page at the top of the current page
$pNum = 0;
while ( $pNum < $numOfPages )
	{
            $pNum = $pNum + 1;
            print "<a href=\"project1.cgi?pageNumber=$pNum\">$pNum </a>";
	    
	}

#figures out what needs to be printed
$toPrint = 0;

#if get method is true then get the parameter number and display only that otherwise print first 5 messages
if ($cgi->request_method() eq "GET")
	{ 
       $pageNumber = $cgi->param("pageNumber");

	   if ($pageNumber < 1)
              {
                  $pageNumber = 0;  
              }
           else
              {
                 $pageNumber = $pageNumber - 1;
              }
           
           $toPrint = $pageNumber * 5;
	}

#cuts off blanks messages when messages not at an even five
$end = $toPrint + 5;
if ($end > $lineCount)
 {
	$end = $lineCount;
 } 

#Creat a list of users
`cat /etc/passwd | cut -f5 -d':' > /tmp/userstemp`;



#Start printing out messages
while( $toPrint < $end )
 {
	#Breaks up each message into its components
	@message = split(/\|/, $allMessages[$toPrint]);      
	
	print "<div class=\"message\">\n";
	print "<p>Title: $message[0]</p>\n";

	#Test to see if it is a user of the system and display message
	open (TEMPFILEIN, "/tmp/userstemp") || warn "Could not open messages file\n";
	$isAUser = 0;
	while (<TEMPFILEIN>)
	 {
 		chomp;
 		if ($message[1] eq $_)
   		 {
     			$isAUser = 1; 
   		 }
	 }
	close (TEMPFILEIN);
	
	# if the author is a user of the system display checkmark otherwise just print name
	if ($isAUser == 1)
   	 {
      		print "<p>Author: $message[1] <img src=\"../lab/messageBoard/check.png\" alt=\"check mark\" /></p>\n";
    	 }
 	else
   	 {
      		print "<p>Author: $message[1]</p>\n";
   	 }

	#Display the rest of the message
	print "<p>Email: $message[2]</p>\n";
	print "<p>Message: $message[3]</p>\n";
	print "<p>Created: $message[4]</p>\n";
	print "<hr />";
	print "</div>\n";
	$toPrint = $toPrint + 1;
 }

close (FILEIN);
print "</div><!-- End of allMessages div -->";
print "<p id=\"check\"><img src=\"../lab/messageBoard/check.png\" alt=\"check mark\" /> indicates users of the hosting system.</p>";
print "</body></html>";
